//
//  CharacterTests.swift
//  StarWarsAppTests
//
//  Created by Rodrigo Gálvez on 22/7/21.
//

import XCTest
@testable import StarWarsApp

class CharacterTests: XCTestCase {

    var sut: Character!

    override func setUpWithError() throws {
        sut = Character.mock()
    }

    override func tearDownWithError() throws {
        sut = nil
    }

    func testgetImageUrl() throws {
        let expectedUrl = "https://starwars-visualguide.com/assets/img/characters/1.jpg"
        let characterImageUrl = sut.getImageUrl()
        XCTAssertEqual(characterImageUrl, expectedUrl)
    }

}
