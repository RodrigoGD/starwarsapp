//
//  CharacterRepositoryTests.swift
//  StarWarsAppTests
//
//  Created by Rodrigo Gálvez on 22/7/21.
//

import XCTest
@testable import StarWarsApp

class CharacterRepositoryTests: XCTestCase {

    var sut: CharacterRepository!
    var persistenceController: PersistenceController!

    override func setUpWithError() throws {
        persistenceController = PersistenceController.preview
        sut = CharacterRepository(persistentController: persistenceController)
    }

    override func tearDownWithError() throws {
        sut = nil
        persistenceController = nil
    }

    func testFetchCharacter() {
        let expected = Character(name: "Character 2", birthYear: "Year 2", eyeColor: "brown", gender: "NoMatch",
                                 hairColor: "blond", height: "300", mass: "600", skinColor: "fair",
                                 homeWorld: "Mars",
                                 films: ["https://swapi.dev/api/films/1/",
                                         "https://swapi.dev/api/films/3/"],
                                 species: ["Specie 1", "Specie 2"],
                                 starships: ["Starship 1", "Starship 2"],
                                 vehicles: ["Vehicle 1", "Vehicle 2"],
                                 url: "https://swapi.dev/api/people/2/")

        let characterFromApi = Character(name: "", birthYear: "", eyeColor: "", gender: "", hairColor: "",
                                         height: "", mass: "", skinColor: "", homeWorld: "",
                                         films: [], species: [], starships: [], vehicles: [],
                                         url: "https://swapi.dev/api/people/2/")

        let character = sut.fetch(character: characterFromApi)

        XCTAssertEqual(character.name, expected.name)
        XCTAssertEqual(character.birthYear, expected.birthYear)
        XCTAssertEqual(character.eyeColor, expected.eyeColor)
        XCTAssertEqual(character.gender, expected.gender)
        XCTAssertEqual(character.hairColor, expected.hairColor)
        XCTAssertEqual(character.height, expected.height)
        XCTAssertEqual(character.mass, expected.mass)
        XCTAssertEqual(character.skinColor, expected.skinColor)
        XCTAssertEqual(character.homeWorld, expected.homeWorld)
        XCTAssertEqual(character.species, expected.species)
        XCTAssertEqual(character.starships, expected.starships)
        XCTAssertEqual(character.vehicles, expected.vehicles)
        XCTAssertEqual(character.url, expected.url)
    }

    func testFetchCharacterFromUrlSuccess() {
        let expected = Character(name: "Character 3", birthYear: "Year 3", eyeColor: "brown", gender: "NoMatch",
                                 hairColor: "blond", height: "300", mass: "600", skinColor: "fair",
                                 homeWorld: "Mars",
                                 films: ["https://swapi.dev/api/films/1/",
                                         "https://swapi.dev/api/films/3/"],
                                 species: ["Specie 1", "Specie 2"],
                                 starships: ["Starship 1", "Starship 2"],
                                 vehicles: ["Vehicle 1", "Vehicle 2"],
                                 url: "https://swapi.dev/api/people/3/")

        let characterUrl = "https://swapi.dev/api/people/3/"

        let character = sut.fetchCharacter(from: characterUrl)

        XCTAssertNotNil(character)
        XCTAssertEqual(character?.name, expected.name)
        XCTAssertEqual(character?.birthYear, expected.birthYear)
        XCTAssertEqual(character?.eyeColor, expected.eyeColor)
        XCTAssertEqual(character?.gender, expected.gender)
        XCTAssertEqual(character?.hairColor, expected.hairColor)
        XCTAssertEqual(character?.height, expected.height)
        XCTAssertEqual(character?.mass, expected.mass)
        XCTAssertEqual(character?.skinColor, expected.skinColor)
        XCTAssertEqual(character?.homeWorld, expected.homeWorld)
        XCTAssertEqual(character?.species, expected.species)
        XCTAssertEqual(character?.starships, expected.starships)
        XCTAssertEqual(character?.vehicles, expected.vehicles)
        XCTAssertEqual(character?.url, expected.url)
    }

    func testFetchCharacterFromUrlWithNoIdExtracted() {
        let characterUrl = ""

        let character = sut.fetchCharacter(from: characterUrl)

        XCTAssertNil(character)
    }

    func testFetchCharacterFromUrlWithNoIdFound() {
        let characterUrl = "https://swapi.dev/api/people/"

        let character = sut.fetchCharacter(from: characterUrl)

        XCTAssertNil(character)
    }

    func testHasDataForCharacter() {
        let character = Character(name: "", birthYear: "", eyeColor: "", gender: "",
                                  hairColor: "", height: "", mass: "", skinColor: "", homeWorld: "",
                                  films: [], species: [], starships: [], vehicles: [],
                                  url: "https://swapi.dev/api/people/5/")
        XCTAssertTrue(sut.hasData(for: character))

        let character2 = Character(name: "", birthYear: "", eyeColor: "", gender: "",
                                  hairColor: "", height: "", mass: "", skinColor: "", homeWorld: "",
                                  films: [], species: [], starships: [], vehicles: [],
                                  url: "https://swapi.dev/api/people/3000/")
        XCTAssertFalse(sut.hasData(for: character2))
    }

    func testSaveCharacter() {
        let expected = Character(name: "name", birthYear: "birthyear", eyeColor: "eyecolor", gender: "gender",
                                  hairColor: "haircolor", height: "height", mass: "mass",
                                  skinColor: "skincolor", homeWorld: "homeworld",
                                  films: [], species: [], starships: [], vehicles: [],
                                  url: "https://swapi.dev/api/people/3000/")
        XCTAssertFalse(sut.hasData(for: expected))

        _ = sut.save(character: expected)
        XCTAssertTrue(sut.hasData(for: expected))

        let fetchedCharacter = sut.fetchCharacter(from: expected.url)
        XCTAssertNotNil(fetchedCharacter)
        XCTAssertEqual(fetchedCharacter?.name, expected.name)
        XCTAssertEqual(fetchedCharacter?.birthYear, expected.birthYear)
        XCTAssertEqual(fetchedCharacter?.eyeColor, expected.eyeColor)
        XCTAssertEqual(fetchedCharacter?.gender, expected.gender)
        XCTAssertEqual(fetchedCharacter?.hairColor, expected.hairColor)
        XCTAssertEqual(fetchedCharacter?.height, expected.height)
        XCTAssertEqual(fetchedCharacter?.mass, expected.mass)
        XCTAssertEqual(fetchedCharacter?.skinColor, expected.skinColor)
        XCTAssertEqual(fetchedCharacter?.homeWorld, expected.homeWorld)
        XCTAssertEqual(fetchedCharacter?.species, expected.species)
        XCTAssertEqual(fetchedCharacter?.starships, expected.starships)
        XCTAssertEqual(fetchedCharacter?.vehicles, expected.vehicles)
        XCTAssertEqual(fetchedCharacter?.url, expected.url)
    }

    func testEditCharacter() {
        let existingCharacter = Character(name: "Character 8", birthYear: "Year 8", eyeColor: "brown", gender: "NoMatch",
                                 hairColor: "blond", height: "300", mass: "600", skinColor: "fair",
                                 homeWorld: "Mars",
                                 films: ["https://swapi.dev/api/films/1/",
                                         "https://swapi.dev/api/films/3/"],
                                 species: ["Specie 1", "Specie 2"],
                                 starships: ["Starship 1", "Starship 2"],
                                 vehicles: ["Vehicle 1", "Vehicle 2"],
                                 url: "https://swapi.dev/api/people/8/")


        let expected = Character(name: "name", birthYear: "birthyear", eyeColor: "eyecolor", gender: "gender",
                                  hairColor: "haircolor", height: "height", mass: "mass",
                                  skinColor: "skincolor", homeWorld: "homeworld",
                                  films: [], species: [], starships: [], vehicles: [],
                                  url: "https://swapi.dev/api/people/8/")

        XCTAssertTrue(sut.hasData(for: existingCharacter))

        let fetchedExistingCharacter = sut.fetchCharacter(from: existingCharacter.url)

        XCTAssertNotNil(fetchedExistingCharacter)
        XCTAssertEqual(fetchedExistingCharacter?.name, existingCharacter.name)
        XCTAssertEqual(fetchedExistingCharacter?.birthYear, existingCharacter.birthYear)
        XCTAssertEqual(fetchedExistingCharacter?.eyeColor, existingCharacter.eyeColor)
        XCTAssertEqual(fetchedExistingCharacter?.gender, existingCharacter.gender)
        XCTAssertEqual(fetchedExistingCharacter?.hairColor, existingCharacter.hairColor)
        XCTAssertEqual(fetchedExistingCharacter?.height, existingCharacter.height)
        XCTAssertEqual(fetchedExistingCharacter?.mass, existingCharacter.mass)
        XCTAssertEqual(fetchedExistingCharacter?.skinColor, existingCharacter.skinColor)
        XCTAssertEqual(fetchedExistingCharacter?.homeWorld, existingCharacter.homeWorld)
        XCTAssertEqual(fetchedExistingCharacter?.species, existingCharacter.species)
        XCTAssertEqual(fetchedExistingCharacter?.starships, existingCharacter.starships)
        XCTAssertEqual(fetchedExistingCharacter?.vehicles, existingCharacter.vehicles)
        XCTAssertEqual(fetchedExistingCharacter?.url, existingCharacter.url)

        _ = sut.edit(character: expected)

        let fetchedCharacter = sut.fetchCharacter(from: expected.url)

        XCTAssertNotNil(fetchedCharacter)
        XCTAssertEqual(fetchedCharacter?.name, expected.name)
        XCTAssertEqual(fetchedCharacter?.birthYear, expected.birthYear)
        XCTAssertEqual(fetchedCharacter?.eyeColor, expected.eyeColor)
        XCTAssertEqual(fetchedCharacter?.gender, expected.gender)
        XCTAssertEqual(fetchedCharacter?.hairColor, expected.hairColor)
        XCTAssertEqual(fetchedCharacter?.height, expected.height)
        XCTAssertEqual(fetchedCharacter?.mass, expected.mass)
        XCTAssertEqual(fetchedCharacter?.skinColor, expected.skinColor)
        XCTAssertEqual(fetchedCharacter?.homeWorld, expected.homeWorld)
        XCTAssertEqual(fetchedCharacter?.species, expected.species)
        XCTAssertEqual(fetchedCharacter?.starships, expected.starships)
        XCTAssertEqual(fetchedCharacter?.vehicles, expected.vehicles)
        XCTAssertEqual(fetchedCharacter?.url, expected.url)
    }

}
