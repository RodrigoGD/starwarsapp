//
//  FilmTests.swift
//  StarWarsAppTests
//
//  Created by Rodrigo Gálvez on 22/7/21.
//

import XCTest
@testable import StarWarsApp

class FilmTests: XCTestCase {

    var sut: Film!

    override func setUpWithError() throws {
        sut = Film.mock()
    }

    override func tearDownWithError() throws {
        sut = nil
    }

    func testgetImageUrl() throws {
        let expectedUrl = "https://starwars-visualguide.com/assets/img/films/1.jpg"
        let characterImageUrl = sut.getImageUrl()
        XCTAssertEqual(characterImageUrl, expectedUrl)
    }

}
