//
//  UtilsTests.swift
//  StarWarsAppTests
//
//  Created by Rodrigo Gálvez on 22/7/21.
//

import XCTest
@testable import StarWarsApp

class UtilsTests: XCTestCase {

    func testExtractIdFromUrlSuccess() throws {
        let expectedId = "82"
        let url = "https://domain.mySite.com/api/v1/82/"
        let id = Utils.extractId(from: url)
        XCTAssertEqual(id, expectedId)

        let expectedId2 = "823"
        let url2 = "https://domain.mySite.com/api/v1/823"
        let id2 = Utils.extractId(from: url2)
        XCTAssertEqual(id2, expectedId2)
    }

    func testExtractIdFromUrlFail() throws {
        let expectedId = "82"
        let url = "https://domain.mySite.com/api/v1/2/"
        let id = Utils.extractId(from: url)
        XCTAssertNotEqual(id, expectedId)

        let expectedId2 = "23"
        let url2 = "https://domain.mySite.com/api/v1/adf"
        let id2 = Utils.extractId(from: url2)
        XCTAssertNotEqual(id2, expectedId2)
    }

}
