//
//  MainView.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 17/7/21.
//

import SwiftUI

struct MainView: View {
    @Environment(\.colorScheme) var systemColorScheme
    @Binding var appColorScheme: ColorScheme?
    var body: some View {
        ZStack {
            TabView {
                FilmView()
                    .tabItem {
                        Text("Films")
                        Image(systemName: "video.circle")
                            .renderingMode(.template)
                    }
                CharacterView()
                    .tabItem {
                        Text("Characters")
                        Image(systemName: "person.2.circle")
                            .renderingMode(.template)
                    }
                SettingsView(colorScheme: $appColorScheme)
                    .tabItem {
                        Text("Settings")
                        Image(systemName: "gear")
                            .renderingMode(.template)
                    }
            }
            .accentColor(Color("AccentColor"))
            .colorScheme(appColorScheme ?? systemColorScheme)
        }
    }
}

struct MainView_Previews: PreviewProvider {
    static var previews: some View {
        MainView(appColorScheme: Binding.constant(nil))
        MainView(appColorScheme: Binding.constant(nil))
            .preferredColorScheme(.dark)
    }
}
