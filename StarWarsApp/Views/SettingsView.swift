//
//  SettingsView.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 21/7/21.
//

import SwiftUI

struct SettingsView: View {
    @State private var selectedAppearance = 0
    @Binding var colorScheme: ColorScheme?
    var body: some View {
        NavigationView {
            VStack {
            Form {
                Section(header: Text("Customization"),
                        footer: Text("Dark side is the recommended appearance.")) {
                    Picker(selection: $selectedAppearance, label: Text("Appearance")) {
                        Text("System Default").tag(0)
                        Text("Light force").tag(1)
                        Text("Dark side").tag(2)
                    }
                    .onChange(of: selectedAppearance) { value in
                        UserDefaults.standard.set(selectedAppearance, forKey: "userColorScheme")
                        switch selectedAppearance {
                        case 0:
                            colorScheme = nil
                        case 1:
                            colorScheme = .light
                        case 2:
                            colorScheme = .dark
                        default:
                            break
                        }
                    }
                }
            }
            }
            .navigationTitle("Settings")
        }
        .onAppear {
            switch colorScheme {
            case .none:
                selectedAppearance = 0
            case .light:
                selectedAppearance = 1
            case .dark:
                selectedAppearance = 2
            default:
                break
            }
        }
    }
}

struct SettingsView_Previews: PreviewProvider {
    static var previews: some View {
        SettingsView(colorScheme: Binding.constant(nil))
        SettingsView(colorScheme: Binding.constant(nil))
            .preferredColorScheme(.dark)
    }
}
