//
//  ImageViews.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

import SwiftUI
import Kingfisher

struct ThumbRectangleImage: View {
    var image: String
    var placeholder: String

    init(image: String, placeholder: String = "films-placeholder") {
        self.image = image
        self.placeholder = placeholder
    }

    var body: some View {
        KFImage.url(URL(string: image))
            .placeholder {
                ThumbRectanglePlaceholder(image: placeholder)
            }
            .fade(duration: 0.25)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .overlay(
                RoundedRectangle(cornerRadius: 2.0)
                    .stroke(lineWidth: 1.5)
                    .foregroundColor(Color("AccentColor"))
            )
    }
}

struct ThumbRectanglePlaceholder: View {
    var image: String
    var body: some View {
        Image(image)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .overlay(
                RoundedRectangle(cornerRadius: 2.0)
                    .stroke(lineWidth: 1.5)
                    .foregroundColor(Color("AccentColor"))
            )
    }
}

struct ThumbCircleImage: View {
    var image: String
    var placeholder: String

    init(image: String, placeholder: String = "character-placeholder") {
        self.image = image
        self.placeholder = placeholder
    }

    var body: some View {
        KFImage.url(URL(string: image))
            .placeholder {
                ThumbCirclePlaceholder(image: placeholder)
            }
            .fade(duration: 0.25)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .clipShape(Circle())
            .overlay(
                Circle()
                    .stroke(Color("AccentColor"), lineWidth: 1.0)
            )
    }
}

struct ThumbCirclePlaceholder: View {
    var image: String

    var body: some View {
        Image(image)
            .resizable()
            .aspectRatio(contentMode: .fit)
            .clipShape(Circle())
            .shadow(color: .black, radius: 20)
            .overlay(
                Circle()
                    .stroke(Color("AccentColor"), lineWidth: 1.0)
            )
    }
}


struct SeparatorView: View {
    var body: some View {
        Rectangle()
            .frame(width: 300, height: 1, alignment: .center)
            .padding(.leading)
            .padding(.trailing)
    }
}

struct ImageViews_Previews: PreviewProvider {
    static var filmImage = Film.mock().getImageUrl()
    static var characterImage = Character.mock().getImageUrl()
    static var previews: some View {
        VStack {
            HStack {
                ThumbRectangleImage(image: filmImage)
                ThumbRectangleImage(image: "")
            }
            HStack {
                ThumbCircleImage(image: characterImage)
                ThumbCircleImage(image: "")
            }
        }

        VStack {
            HStack {
                ThumbRectangleImage(image: filmImage)
                    .preferredColorScheme(.dark)
                ThumbRectangleImage(image: "")
                    .preferredColorScheme(.dark)
            }
            HStack {
                ThumbCircleImage(image: characterImage)
                    .preferredColorScheme(.dark)
                ThumbCircleImage(image: "")
                    .preferredColorScheme(.dark)
            }
        }
    }
}
