//
//  TextViews.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

import SwiftUI

struct ListTitleText: View {
    var text: String
    var body: some View {
        Text(text)
            .bold()
            .font(.title2)
            .foregroundColor(Color("TextColor"))
    }
}

struct ListDescriptionText: View {
    var text: String
    var body: some View {
        Text(text)
            .kerning(1.0)
            .font(.caption)
    }
}

struct ListFootnoteText: View {
    var text: String
    var body: some View {
        Text(text)
            .font(.caption2)
    }
}

struct ListLinkToDetailText: View {
    var text: String

    init(text: String = "More >") {
        self.text = text
    }

    var body: some View {
        Text(text)
            .font(.footnote)
            .foregroundColor(Color("AccentColor"))
    }
}

struct FieldInfoTextView: View {
    var field: String
    var text: String
    var body: some View {
        HStack {
            Text("\(field): ")
                .fontWeight(.semibold)
            Text(text)
                .fontWeight(.light)
        }
        .font(.callout)
    }
}

struct LoadingView: View {
    var text: String
    var body: some View {
        HStack {
            ProgressView()
                .progressViewStyle(CircularProgressViewStyle())
            Text(" \(text)")
        }
    }
}

struct TextViews_Preview: PreviewProvider {
    static var previews: some View {
        VStack(spacing: 10) {
            FieldInfoTextView(field: "Field", text: "Text")
            ListTitleText(text: "Text")
            ListDescriptionText(text: "Text")
            ListFootnoteText(text: "Text")
            ListLinkToDetailText(text: "Text")
            LoadingView(text: "loading...")
        }
    }
}
