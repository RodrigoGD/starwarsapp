//
//  CharacterDetailView.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

import SwiftUI

struct CharacterDetailView: View {
    @ObservedObject var characterViewModel: CharacterViewModel
    @State var isPresented: Bool
    @State var isEdited: Bool
    let isPreview: Bool

    init(character: Character,
         characterViewModel: CharacterViewModel = CharacterViewModel(),
         isPreview: Bool = false) {
        self.characterViewModel = characterViewModel
        self.isPreview = isPreview
        self.isEdited = false
        self.isPresented = false
        self.characterViewModel.state = .Fetched(character)
    }

    var body: some View {
        ScrollView {
            Group { () -> AnyView in
                switch characterViewModel.state {
                case .Init:
                    return AnyView(Text("Init state"))
                case .Loading(let message):
                    return AnyView(LoadingView(text: message))
                case .Fetched(let character):
                    return fetchView(with: character)
                case .NoResultsFound:
                    return AnyView(Text("No results found state"))
                case .ApiError(let message):
                    return AnyView(Text(message))
                }
            }

        }
    }

    private func fetchView(with character: Character) -> AnyView {
        return AnyView(CharacterDetail(character: character, isPreview: isPreview)
                        .navigationTitle(character.name)
                        .navigationBarItems(trailing:
                                                Button(action: { self.isPresented = true }) {
                                                    Image(systemName: "rectangle.and.pencil.and.ellipsis")
                                                }
                                                .sheet(isPresented: $isPresented) {
                                                    CharacterEditView(character: character) { editedCharacter in
                                                        self.characterViewModel.save(character: editedCharacter)
                                                        self.isEdited = true
                                                        self.isPresented = false
                                                    }
                                                }
                        )
        )
    }
}

struct CharacterDetail: View {
    let character: Character
    let isPreview: Bool
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            HStack(alignment: .center) {
                Spacer()
                ThumbCircleImage(image: character.getImageUrl())
                    .frame(width: 120, height: 120)
                Spacer()
            }
            .padding(.top)

            ListTitleText(text: character.name)
                .padding(.top)
                .padding(.leading)
            SeparatorView()
            VStack(alignment: .leading, spacing: 2) {
                FieldInfoTextView(field: "Gender",
                                  text: character.gender.capitalized)
                FieldInfoTextView(field: "Born on",
                                  text: character.birthYear)
                FieldInfoTextView(field: "Height",
                                  text: "\(character.height)ft")
                FieldInfoTextView(field: "Mass",
                                  text: "\(character.mass)lb")
                FieldInfoTextView(field: "Skin Color",
                                  text: character.skinColor.capitalized)
                FieldInfoTextView(field: "Hair color",
                                  text: character.hairColor.capitalized)
                FieldInfoTextView(field: "Eye color",
                                  text: character.eyeColor.capitalized)

            }
            .padding(.leading)
            if character.films.count != 0 {
                ListTitleText(text: "Character appears on")
                    .padding(.top)
                    .padding(.leading)
                SeparatorView()
                ForEach(character.films.indices) { index in
                    if isPreview {
                        FilmRowView(film: Film.mock(), rowStyle: .minimal)
                            .padding(.leading)
                            .padding(.trailing)
                    } else {
                        let filmUrl = character.films[index]
                        FilmRowView(from: filmUrl, rowStyle: .minimal)
                            .padding(.leading)
                            .padding(.trailing)
                    }
                }
            }
            Spacer()
        }
        .background(Color("BackgroundColor"))
    }
}

struct CharacterDetailView_Previews: PreviewProvider {
    static let character: Character = Character.mock()
    static var previews: some View {
        CharacterDetailView(character: character,
                            characterViewModel: CharacterViewModel(state: .Fetched(character)),
                            isPreview: true)
        CharacterDetailView(character: character,
                            characterViewModel: CharacterViewModel(state: .Fetched(character)),
                            isPreview: true)
            .preferredColorScheme(.dark)
    }
}
