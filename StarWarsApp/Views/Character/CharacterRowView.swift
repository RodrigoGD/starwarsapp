//
//  CharacterRowView.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

import SwiftUI

struct CharacterRowView: View {
    @ObservedObject var characterViewModel: CharacterViewModel
    @State var _flag: Bool
    var character: Character!
    var url: String!

    init(character: Character,
         characterViewModel: CharacterViewModel = CharacterViewModel()) {
        self.characterViewModel = characterViewModel
        self.character = character
        self.url = nil
        self._flag = false
    }

    init(from url: String,
         characterViewModel: CharacterViewModel = CharacterViewModel()) {
        self.characterViewModel = characterViewModel
        self.character = nil
        self.url = url
        self._flag = false
    }

    var body: some View {

        Group { () -> AnyView in
            switch characterViewModel.state {
            case .Init:
                return AnyView(Text("Init state"))
            case .Loading(let message):
                return AnyView(LoadingView(text: message))
            case .Fetched(let aCharacter):
                return AnyView(CharacterRow(characterViewModel: characterViewModel, character: aCharacter))
            case .NoResultsFound:
                return AnyView(Text("No results found state"))
            case .ApiError(let message):
                return AnyView(Text(message))
            }
        }
        .onAppear() {
            if character == nil && !_flag {
                characterViewModel.getCharacter(from: url)
            } else if character != nil && !_flag {
                characterViewModel.state = .Fetched(character)
            }
            _flag = true
        }
    }
}

struct CharacterRow: View {
    @ObservedObject var characterViewModel: CharacterViewModel
    var character: Character

    var body: some View {
        ZStack {
            Color("BackgroundColor")
            HStack(spacing: 10) {
                ThumbCircleImage(image: character.getImageUrl())
                    .frame(width: 40, height: 40)
                    .padding(10)

                HStack {
                    VStack(alignment: .leading, spacing: -5) {
                        ListTitleText(text: character.name)
                            .padding(.init(top: 10, leading: 0, bottom: 0, trailing: 0))
                        HStack {
                            ListFootnoteText(text: character.gender.capitalized)
                            Spacer()
                            ListFootnoteText(text: "Born on: \(character.birthYear)")
                            Spacer()
                            NavigationLink(destination:
                                            CharacterDetailView(character: character)
                                            .navigationTitle(character.name)
                                            .navigationBarTitleDisplayMode(.large)
                                            .onDisappear(perform: {
                                                self.characterViewModel.getCharacter(from: character.url)
                                            })
                            ) { ListLinkToDetailText() }
                        }
                        .padding(.init(top: 10, leading: 0, bottom: 5, trailing: 10))
                    }
                }
            }
        }
        .frame(maxWidth: .infinity)
        .foregroundColor(Color("TextColor"))
    }
}

struct CharacterRowView_Previews: PreviewProvider {
    static let character: Character = Character.mock()
    static var previews: some View {
        CharacterRowView(character: character)
        CharacterRowView(character: character)
            .preferredColorScheme(.dark)
    }
}
