//
//  CharactersView.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 17/7/21.
//

import SwiftUI

struct CharacterView: View {
    @ObservedObject var characterListViewModel: CharacterListViewModel
    private let isPreview: Bool

    init(characterListViewModel: CharacterListViewModel = CharacterListViewModel(), isPreview: Bool = false) {
        self.characterListViewModel = characterListViewModel
        self.isPreview = isPreview
    }

    var body: some View {
        NavigationView {
            Group { () -> AnyView in
                switch characterListViewModel.state {
                case .Init:
                    return AnyView(Text("Init state"))
                case .Loading(let message):
                    return AnyView(LoadingView(text: message))
                case .Fetched(let response):
                    return AnyView(CharacterListView(characters: response.characters))
                case .NoResultsFound:
                    return AnyView(Text("No results found state"))
                case .ApiError(let message):
                    return AnyView(Text(message))
                }
            }
            .navigationTitle("Characters")
            .navigationBarTitleDisplayMode(.large)
        }
        .onAppear {
            if !isPreview {
                characterListViewModel.getCharacters()
            }
        }
    }
}

struct CharacterListView: View {
    let characters: [Character]
    var body: some View {
        ScrollView {
            ZStack {
                Color(.black)
                VStack(spacing: 1) {
                    ForEach(characters.indices) { index in
                        let character = characters[index]
                        CharacterRowView(character: character)
                    }
                    .fixedSize(horizontal: false, vertical: true)
                }
            }
        }
    }
}

struct CharacterView_Previews: PreviewProvider {
    static let response = CharacterListResponse.mock()
    static let characterListViewModel = CharacterListViewModel(state: .Fetched(response))
    static var previews: some View {
        Group {
            CharacterView(characterListViewModel: characterListViewModel, isPreview: true)
            CharacterView(characterListViewModel: characterListViewModel, isPreview: true)
                .preferredColorScheme(.dark)
        }
    }
}
