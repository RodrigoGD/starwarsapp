//
//  CharacterEditView.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 19/7/21.
//

import SwiftUI

struct CharacterEditView: View {
    var character: Character
    @State var name: String
    @State var gender: String
    @State var birthYear: String
    @State var height: String
    @State var mass: String
    @State var skinColor: String
    @State var hairColor: String
    @State var eyeColor: String

    let onComplete: (Character?) -> Void

    init(character: Character, onComplete: @escaping (Character?) -> Void) {
        self.character = character
        self.name = character.name
        self.gender = character.gender
        self.birthYear = character.birthYear
        self.height = character.height
        self.mass = character.mass
        self.skinColor = character.skinColor
        self.hairColor = character.hairColor
        self.eyeColor = character.eyeColor
        self.onComplete = onComplete
    }

    var body: some View {
        NavigationView {
            Form {
                Section(header: Text("Name")) {
                    TextField("Name", text: $name)
                }
                Section(header: Text("Gender")) {
                    TextField("Gender", text: $gender)
                }
                Section(header: Text("Birth year")) {
                    TextField("Birth year", text: $birthYear)
                }
                Section(header: Text("Height")) {
                    TextField("Height", text: $height)
                }
                Section(header: Text("Mass")) {
                    TextField("Mass", text: $mass)
                }
                Section(header: Text("Skin color")) {
                    TextField("Skin color", text: $skinColor)
                }
                Section(header: Text("Hair color")) {
                    TextField("Hair color", text: $hairColor)
                }
                Section(header: Text("Eye Color")) {
                    TextField("Eye color", text: $eyeColor)
                }
                Section {
                    Button(action: editCharacterAction) {
                      Text("Confirm")
                    }
                }
            }
            .navigationBarTitle(Text("Edit Character"), displayMode: .inline)
            .navigationBarItems(trailing:Button(action: { onComplete(nil) }) {Text("Cancel")})
        }
    }

    private func editCharacterAction() {
        let characterEdited = Character(name: name,
                                        birthYear: birthYear,
                                        eyeColor: eyeColor,
                                        gender: gender,
                                        hairColor: hairColor,
                                        height: height,
                                        mass: mass,
                                        skinColor: skinColor,
                                        homeWorld: character.homeWorld,
                                        films: character.films,
                                        species: character.species,
                                        starships: character.starships,
                                        vehicles: character.vehicles,
                                        url: character.url)
        onComplete(characterEdited)
    }
}

struct CharacterEditView_Previews: PreviewProvider {
    static let character = Character.mock()
    static let onComplete: ((Character?) -> Void) = { _ in }
    static var previews: some View {
        Group {
            CharacterEditView(character: character, onComplete: onComplete)
            CharacterEditView(character: character, onComplete: onComplete)
                .preferredColorScheme(.dark)
        }
    }
}
