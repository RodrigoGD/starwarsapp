//
//  FilmView.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 17/7/21.
//

import SwiftUI

struct FilmView: View {
    @ObservedObject var filmListViewModel: FilmListViewModel
    private let isPreview: Bool

    init(filmListViewModel: FilmListViewModel = FilmListViewModel(), isPreview: Bool = false) {
        self.filmListViewModel = filmListViewModel
        self.isPreview = isPreview
    }

    var body: some View {
        NavigationView {
            Group { () -> AnyView in
                switch filmListViewModel.state {
                case .Init:
                    return AnyView(Text("Init state"))
                case .Loading(let message):
                    return AnyView(LoadingView(text: message))
                case .Fetched(let response):
                    return AnyView(FilmListView(films: response.films))
                case .NoResultsFound:
                    return AnyView(Text("No results found state"))
                case .ApiError(let message):
                    return AnyView(Text(message))
                }
            }
            .navigationTitle("Films")
            .navigationBarTitleDisplayMode(.large)
        }
        .onAppear {
            if !isPreview {
                filmListViewModel.getFilms()
            }
        }
    }
}

struct FilmListView: View {
    let films: [Film]
    var body: some View {
        ScrollView {
            ZStack {
                Color(.black)
                VStack(spacing: 1) {
                    ForEach(films.indices) { index in
                        let film = films[index]
                        FilmRowView(film: film, rowStyle: .compact)
                    }
                    .frame(height: 150)
                }
            }
        }

    }
}

struct FilmView_Previews: PreviewProvider {
    static let response = FilmListResponse.mock()
    static let filmListViewModel = FilmListViewModel(state: .Fetched(response))
    static var previews: some View {
        FilmView(filmListViewModel: filmListViewModel, isPreview: true)
        FilmView(filmListViewModel: filmListViewModel, isPreview: true)
            .preferredColorScheme(.dark)
    }
}
