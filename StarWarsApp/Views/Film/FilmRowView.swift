//
//  FilmRowView.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

import SwiftUI

struct FilmRowView: View {
    @ObservedObject var filmViewModel: FilmViewModel
    @State var _flag: Bool
    var film: Film!
    var url: String!
    var rowStyle: RowStyle

    init(film: Film,
         filmViewModel: FilmViewModel = FilmViewModel(),
         moreInfo: Bool = true,
         rowStyle: RowStyle) {
        self.filmViewModel = filmViewModel
        self.film = film
        self.url = nil
        self.rowStyle = rowStyle
        self._flag = false
    }

    init(from url: String,
         filmViewModel: FilmViewModel = FilmViewModel(),
         moreInfo: Bool = true,
         rowStyle: RowStyle) {
        self.filmViewModel = filmViewModel
        self.film = nil
        self.url = url
        self.rowStyle = rowStyle
        self._flag = false
    }

    var body: some View {
        Group { () -> AnyView in
            switch filmViewModel.state {
            case .Init:
                return AnyView(Text("Init state"))
            case .Loading(let message):
                return AnyView(LoadingView(text: message))
            case .Fetched(let aFilm):
                return AnyView(FilmRow(film: aFilm, rowStyle: rowStyle))
            case .NoResultsFound:
                return AnyView(Text("No results found state"))
            case .ApiError(let message):
                return AnyView(Text(message))
            }
        }
        .onAppear() {
            if film == nil && !_flag {
                _flag = true
                filmViewModel.getFilm(from: url)
            } else if film != nil {
                filmViewModel.state = .Fetched(film)
            }
        }
    }
}

struct FilmRow: View {
    var film: Film
    var rowStyle: RowStyle
    var body: some View {
        ZStack {
            Color("BackgroundColor")
            HStack(alignment: .top, spacing: 10) {
                ThumbRectangleImage(image: film.getImageUrl())
                    .frame(width: 70, height: 100)
                    .padding(10)

                VStack(alignment: .leading, spacing: 5) {
                    ListTitleText(text: film.title)
                        .padding(.top, 5)
                    switch rowStyle {
                    case .full:
                        FilmRowViewFull(film: film)
                    case .compact:
                        FilmRowViewCompact(film: film)
                    case .minimal:
                        HStack {
                        FilmRowViewMinimal(film: film)
                            .padding(.top, -5)
                            Spacer()
                        }
                    }
                }
            }
            .foregroundColor(Color("TextColor"))
        }
    }
}

struct FilmRowViewFull: View {
    var film: Film
    var body: some View {
        ListFootnoteText(text: "Episode \(film.episodeId)")
        ListDescriptionText(text: film.openingCrawl)
        HStack {
            Spacer()
            VStack(alignment: .trailing) {
                ListFootnoteText(text: "Released on")
                ListFootnoteText(text: film.releaseDate)
                    .padding(.bottom, 1)
                ListFootnoteText(text: "Directed by")
                ListFootnoteText(text: film.director)
                    .padding(.bottom, 1)
                ListFootnoteText(text: "Producer by")
                ListFootnoteText(text: film.producer)
            }
        }
        .padding(.top, 10)
        .padding(.trailing, 10)
        .padding(.bottom, 5)
    }
}

struct FilmRowViewCompact: View {
    var film: Film
    var body: some View {
        ListDescriptionText(text: film.openingCrawl)
        HStack {
            ListFootnoteText(text: "Released: \(film.releaseDate)")
            Spacer()
            FilmNavigationLinkView(film: film, text: "More >")
        }
        .padding(.top, 10)
        .padding(.trailing, 10)
        .padding(.bottom, 5)
    }
}

struct FilmRowViewMinimal: View {
    var film: Film
    var body: some View {
        VStack(alignment: .leading, spacing: 5) {
            ListFootnoteText(text: "Episode \(film.episodeId)")
            ListFootnoteText(text: "Directed by\n\(film.director)")
            Spacer()
            FilmNavigationLinkView(film: film,
                                   text: "Read full description >")
        }
        .fixedSize(horizontal: false, vertical: true)
    }
}

struct FilmNavigationLinkView: View {
    var film: Film
    var text: String
    var body: some View {
        NavigationLink(
            destination:
                FilmDetailView(film: film)
                .navigationBarTitle(film.title,
                                    displayMode: .large),
            label: {
                ListLinkToDetailText(text: text)
            })
    }
}

struct FilmRowView_Previews: PreviewProvider {
    static let film: Film = Film.mock()
    static var previews: some View {
        FilmRowView(film: film, rowStyle: .full)
        FilmRowView(film: film, rowStyle: .full)
            .preferredColorScheme(.dark)
        FilmRowView(film: film, rowStyle: .compact)
        FilmRowView(film: film, rowStyle: .compact)
            .preferredColorScheme(.dark)
        FilmRowView(film: film, rowStyle: .minimal)
        FilmRowView(film: film, rowStyle: .minimal)
            .preferredColorScheme(.dark)
    }
}
