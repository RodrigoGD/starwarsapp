//
//  FilmDetailView.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

import SwiftUI

struct FilmDetailView: View {
    let film: Film
    let isPreview: Bool

    init(film: Film, isPreview: Bool = false) {
        self.film = film
        self.isPreview = isPreview
    }

    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 5) {
                FilmRowView(film: film, rowStyle: .full)
                    .fixedSize(horizontal: false, vertical: true)
                    .padding(.leading)
                    .padding(.trailing)
                if film.characters.count != 0 {
                    ListTitleText(text: "Film's characters")
                        .padding(.top)
                        .padding(.leading)
                    SeparatorView()
                    ForEach(film.characters.indices) { index in
                        if isPreview {
                            CharacterRowView(character: Character.mock())
                                .padding(.leading)
                                .padding(.trailing)
                        } else {
                            let characterUrl = film.characters[index]
                            CharacterRowView(from: characterUrl)
                                .padding(.leading)
                                .padding(.trailing)
                        }
                    }
                }
                Spacer()
            }.background(Color("BackgroundColor"))
        }
    }
}

struct FilmDetailView_Previews: PreviewProvider {
    static let film: Film = Film.mock()
    static var previews: some View {
        FilmDetailView(film: film, isPreview: true)
        FilmDetailView(film: film, isPreview: true)
            .preferredColorScheme(.dark)
    }
}
