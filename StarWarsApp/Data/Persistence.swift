//
//  Persistence.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 17/7/21.
//

import CoreData

struct PersistenceController {
    static let shared = PersistenceController()

    static var preview: PersistenceController = {
        let result = PersistenceController(inMemory: true)
        let viewContext = result.container.viewContext
        for index in 2...10 {
            let newCharacter = CharacterDB(context: viewContext)
            newCharacter.id = "\(index)"
            newCharacter.name = "Character \(index)"
            newCharacter.birthYear = "Year \(index)"
            newCharacter.eyeColor = "brown"
            newCharacter.gender = "NoMatch"
            newCharacter.hairColor = "blond"
            newCharacter.skinColor = "fair"
            newCharacter.height = "300"
            newCharacter.mass = "600"
            newCharacter.url = "\(Constants.baseURL)\(Constants.characterAppend)/\(index)/"
            newCharacter.homeWorld = "Mars"
            newCharacter.species = ["Specie 1", "Specie 2"]
            newCharacter.films = ["\(Constants.baseURL)\(Constants.filmAppend)/1/",
                                  "\(Constants.baseURL)\(Constants.filmAppend)/3/"]
            newCharacter.starships = ["Starship 1", "Starship 2"]
            newCharacter.vehicles = ["Vehicle 1", "Vehicle 2"]
        }
        do {
            try viewContext.save()
        } catch {
            // Replace this implementation with code to handle the error appropriately.
            // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
        return result
    }()

    let container: NSPersistentContainer

    init(inMemory: Bool = false) {
        container = NSPersistentContainer(name: "StarWarsApp")
        if inMemory {
            container.persistentStoreDescriptions.first!.url = URL(fileURLWithPath: "/dev/null")
        }
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

                /*
                Typical reasons for an error here include:
                * The parent directory does not exist, cannot be created, or disallows writing.
                * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                * The device is out of space.
                * The store could not be migrated to the current model version.
                Check the error message to determine what the actual problem was.
                */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
    }
}
