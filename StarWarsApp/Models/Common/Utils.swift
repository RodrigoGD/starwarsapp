//
//  Utils.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 21/7/21.
//

import Foundation

struct Utils {
    static func extractId(from url: String) -> String? {
        return url.split(separator: "/").compactMap ({ $0.description }).last
    }
}
