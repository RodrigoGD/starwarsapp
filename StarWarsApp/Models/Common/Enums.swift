//
//  Enums.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

enum Constants {
    static var baseURL = "https://swapi.dev/api/"
    static var characterAppend = "people"
    static var filmAppend = "films"
    static var baseImagesURL = "https://starwars-visualguide.com/assets/img/"
    static var characterImagesAppend = "characters"
}

enum ResponseState<T> {
    case Init
    case Loading(String)
    case Fetched(T)
    case NoResultsFound
    case ApiError(String)
}

enum RowStyle {
    case full
    case compact
    case minimal
}
