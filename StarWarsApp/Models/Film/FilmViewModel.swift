//
//  FilmViewModel.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 19/7/21.
//

import Foundation
import Combine

class FilmViewModel: ObservableObject {
    @Published var state: ResponseState<Film> = .Init

    var subscription: AnyCancellable?

    let repository: FilmRepository!

    init(repository: FilmRepository = FilmRepository(),
         state: ResponseState<Film> = .Init) {
        self.repository = repository
        self.state = state
    }
}

extension FilmViewModel {
    func getFilm(from url: String) {
        state = .Loading("Film is loading.")
        subscription = repository.getFilm(from: url)
            .sink(receiveCompletion: { completion in
            if case let .failure(error) = completion {
                self.state = .ApiError(error.localizedDescription)
            }
        }, receiveValue: { response in
            self.state = .Fetched(response)
        })
    }
}
