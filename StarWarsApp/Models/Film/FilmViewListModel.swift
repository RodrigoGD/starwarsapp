//
//  FilmListViewModel.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 17/7/21.
//

import Foundation
import Combine

class FilmListViewModel: ObservableObject {
    @Published var state: ResponseState<FilmListResponse> = .Init

    var subscription: AnyCancellable?

    let repository: FilmRepository!

    init(repository: FilmRepository = FilmRepository(),
         state: ResponseState<FilmListResponse> = .Init) {
        self.repository = repository
        self.state = state
    }
}

extension FilmListViewModel {
    func getFilms() {
        state = .Loading("Films loading...")
        subscription = repository.getAllFilms()
            .sink(receiveCompletion: { completion in
                if case let .failure(error) = completion {
                    self.state = .ApiError(error.localizedDescription)
                }
            }, receiveValue: { response in
                if response.films.count == 0 {
                    self.state = .NoResultsFound
                } else {
                    self.state = .Fetched(response)
                }
            })
    }
}
