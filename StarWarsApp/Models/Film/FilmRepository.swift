//
//  FilmRepository.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 17/7/21.
//

import Foundation
import Combine

class FilmRepository {
    let apiClient: ApiClient!

    init(apiClient: ApiClient = ApiClient()) {
        self.apiClient = apiClient
    }

    func getAllFilms() -> AnyPublisher<FilmListResponse, Error> {
        let url = Constants.baseURL + Constants.filmAppend
        guard let urlRequest = URL(string: url) else {
            return AnyPublisher(Fail<FilmListResponse, Error>(error: URLError(.badURL)))
        }
        let request = URLRequest(url: urlRequest)

        return apiClient.run(request)
            .map(\.value)
            .eraseToAnyPublisher()
    }

    func getFilm(from url: String) -> AnyPublisher<Film, Error> {
        guard let requestUrl = URL(string: url) else {
            return AnyPublisher(Fail<Film, Error>(error: URLError(.badURL)))
        }
        let request = URLRequest(url: requestUrl)

        return apiClient.run(request)
            .map(\.value)
            .eraseToAnyPublisher()
    }
}
