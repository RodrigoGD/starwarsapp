//
//  FilmListResponse.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 21/7/21.
//

import Foundation

struct FilmListResponse: Codable {
    var films: [Film]

    enum CodingKeys: String, CodingKey {
        case films = "results"
    }
}

extension FilmListResponse {
    static func mock(items: Int = 5) -> FilmListResponse {
        var films = [Film]()
        for _ in 0..<items {
            films.append(Film.mock())
        }
        return FilmListResponse(films: films)
    }
}
