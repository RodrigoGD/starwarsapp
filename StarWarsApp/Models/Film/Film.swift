//
//  Film.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 17/7/21.
//
import Foundation

struct Film: Codable {
    let title: String
    let episodeId: Int
    let openingCrawl: String
    let director: String
    let producer: String
    let releaseDate: String
    let species: [String]
    let starships: [String]
    let vehicles: [String]
    let characters: [String]
    let planets: [String]
    let url: String

    enum CodingKeys: String, CodingKey {
        case title = "title"
        case episodeId = "episode_id"
        case openingCrawl = "opening_crawl"
        case director = "director"
        case producer = "producer"
        case releaseDate = "release_date"
        case species = "species"
        case starships = "starships"
        case vehicles = "vehicles"
        case characters = "characters"
        case planets = "planets"
        case url = "url"
    }
}

// MARK: - Statics
extension Film {
    static func mock() -> Film {
        return Film(title: "A new hope",
                    episodeId: 1,
                    openingCrawl: "It is a period of civil war.\r\nRebel spaceships, striking\r\nfrom a hidden base, have won\r\ntheir first victory against\r\nthe evil Galactic Empire.\r\n\r\nDuring the battle, Rebel\r\nspies managed to steal secret\r\nplans to the Empire's\r\nultimate weapon, the DEATH\r\nSTAR, an armored space\r\nstation with enough power\r\nto destroy an entire planet.\r\n\r\nPursued by the Empire's\r\nsinister agents, Princess\r\nLeia races home aboard her\r\nstarship, custodian of the\r\nstolen plans that can save her\r\npeople and restore\r\nfreedom to the galaxy....",
                    director: "George Lucas",
                    producer: "Gary Kurtz, Rick McCallum",
                    releaseDate: "1977-05-25",
                    species: ["https://swapi.dev/api/species/1/",
                              "https://swapi.dev/api/species/2/",
                              "https://swapi.dev/api/species/3/",
                              "https://swapi.dev/api/species/4/",
                              "https://swapi.dev/api/species/5/"],
                    starships: ["https://swapi.dev/api/starships/2/",
                                "https://swapi.dev/api/starships/3/",
                                "https://swapi.dev/api/starships/5/"],
                    vehicles: ["https://swapi.dev/api/vehicles/4/",
                               "https://swapi.dev/api/vehicles/6/",
                               "https://swapi.dev/api/vehicles/8/"],
                    characters: ["https://swapi.dev/api/people/1/",
                                 "https://swapi.dev/api/people/2/",
                                 "https://swapi.dev/api/people/3/"],
                    planets: ["https://swapi.dev/api/planets/1/",
                              "https://swapi.dev/api/planets/2/",
                              "https://swapi.dev/api/planets/3/"],
                    url: "https://swapi.dev/api/films/1/")
    }
}

// MARK: - Functions
extension Film {
    func getImageUrl() -> String {
        guard let id = Utils.extractId(from: self.url) else { return "" }
        return "\(Constants.baseImagesURL)\(Constants.filmAppend)/\(id).jpg"
    }
}
