//
//  Character.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

import Foundation

struct Character: Codable {
    let name: String
    let birthYear: String
    let eyeColor: String
    let gender: String
    let hairColor: String
    let height: String
    let mass: String
    let skinColor: String
    let homeWorld: String
    let films: [String]
    let species: [String]
    let starships: [String]
    let vehicles: [String]
    let url: String

    enum CodingKeys: String, CodingKey {
        case name = "name"
        case birthYear = "birth_year"
        case eyeColor = "eye_color"
        case gender = "gender"
        case hairColor = "hair_color"
        case height = "height"
        case mass = "mass"
        case skinColor = "skin_color"
        case homeWorld = "homeworld"
        case films = "films"
        case species = "species"
        case starships = "starships"
        case vehicles = "vehicles"
        case url = "url"
    }
}

// MARK: - Statics
extension Character {
    static func map(from characterDB: CharacterDB) -> Character {
        return Character(name: characterDB.name ?? "",
                         birthYear: characterDB.birthYear ?? "",
                         eyeColor: characterDB.eyeColor ?? "",
                         gender: characterDB.gender ?? "",
                         hairColor: characterDB.hairColor ?? "",
                         height: characterDB.height ?? "",
                         mass: characterDB.mass ?? "",
                         skinColor: characterDB.skinColor ?? "",
                         homeWorld: characterDB.homeWorld ?? "",
                         films: characterDB.films ?? [],
                         species: characterDB.species ?? [],
                         starships: characterDB.starships ?? [],
                         vehicles: characterDB.vehicles ?? [],
                         url: characterDB.url ?? "")
    }
    
    static func mock() -> Character {
        return Character(name: "Luke Skywalker",
                         birthYear: "19BBY",
                         eyeColor: "blue",
                         gender: "male",
                         hairColor: "blond",
                         height: "172",
                         mass: "77",
                         skinColor: "fair",
                         homeWorld: "https://swapi.dev/api/planets/1/",
                         films: ["https://swapi.dev/api/films/1/",
                                 "https://swapi.dev/api/films/2/",
                                 "https://swapi.dev/api/films/3/",
                                 "https://swapi.dev/api/films/6/"],
                         species: [],
                         starships: ["https://swapi.dev/api/starships/12/",
                                     "https://swapi.dev/api/starships/22/"],
                         vehicles: ["https://swapi.dev/api/vehicles/14/",
                                    "https://swapi.dev/api/vehicles/30/"],
                         url: "https://swapi.dev/people/1/")
    }
}

// MARK: - Functions
extension Character {
    func getImageUrl() -> String {
        guard let id = Utils.extractId(from: self.url) else { return "" }
        return "\(Constants.baseImagesURL)\(Constants.characterImagesAppend)/\(id).jpg"
    }
}
