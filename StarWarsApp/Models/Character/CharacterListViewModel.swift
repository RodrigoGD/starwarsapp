//
//  CharacterListViewModel.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

import Foundation
import Combine

class CharacterListViewModel: ObservableObject {
    @Published var state: ResponseState<CharacterListResponse> = .Init

    var subscription: AnyCancellable?

    let repository: CharacterRepository!

    init(repository: CharacterRepository = CharacterRepository(),
         state: ResponseState<CharacterListResponse> = .Init) {
        self.repository = repository
        self.state = state
    }
}

extension CharacterListViewModel {
    func getCharacters() {
        state = .Loading("Characters loading...")
        subscription = repository.getAllCharacters()
            .sink(receiveCompletion: { completion in
            if case let .failure(error) = completion {
                self.state = .ApiError(error.localizedDescription)
            }
        }, receiveValue: { response in
            if response.characters.count == 0 {
                self.state = .NoResultsFound
            } else {
                let mergedCharacters = response.characters.map { self.repository.fetch(character: $0) }
                let mergeData = CharacterListResponse(characters: mergedCharacters,
                                                      next: response.next,
                                                      previous: response.previous)
                self.state = .Fetched(mergeData)
            }
        })
    }

    func refreshCharacters() {
        
    }

    private func mergeWithLocalData(for characters: [Character]) -> [Character] {
        return characters.map { repository.fetch(character: $0) }
    }
}
