//
//  CharacterViewModel.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

import Foundation
import Combine

class CharacterViewModel: ObservableObject {
    @Published var state: ResponseState<Character> = .Init

    var subscription: AnyCancellable?

    let repository: CharacterRepository!

    init(repository: CharacterRepository = CharacterRepository(),
         state: ResponseState<Character> = .Init) {
        self.repository = repository
        self.state = state
    }
}

extension CharacterViewModel {
    func getCharacter(from url: String) {
        state = .Loading("Character is loading.")
        guard hasData(for: url) else {
            retrieve(from: url)
            return
        }
    }

    private func hasData(for url: String) -> Bool {
        if let character = repository.fetchCharacter(from: url) {
            state = .Fetched(character)
            return true
        }
        return false
    }

    private func retrieve(from url: String) {
        subscription = repository.getCharacter(from: url)
            .sink(receiveCompletion: { completion in
            if case let .failure(error) = completion {
                self.state = .ApiError(error.localizedDescription)
            }
        }, receiveValue: { response in
            self.state = .Fetched(response)
        })
    }

    func save(character: Character?) {
        guard let aCharacter = character else {
            return
        }

        state = .Loading("Saving changes, please wait.")

        if repository.hasData(for: aCharacter) {
            subscription = repository.edit(character: aCharacter)
                .sink(receiveCompletion: { completion in
                if case let .failure(error) = completion {
                    self.state = .ApiError(error.localizedDescription)
                }
            }, receiveValue: { response in
                self.state = .Fetched(response)
            })
        } else {
            subscription = repository.save(character: aCharacter)
                .sink(receiveCompletion: { completion in
                if case let .failure(error) = completion {
                    self.state = .ApiError(error.localizedDescription)
                }
            }, receiveValue: { response in
                self.state = .Fetched(response)
            })
        }
    }
}
