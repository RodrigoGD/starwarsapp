//
//  CharacterListResponse.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 21/7/21.
//

import Foundation

struct CharacterListResponse: Codable {
    var characters: [Character]
    let next: String?
    let previous: String?

    enum CodingKeys: String, CodingKey {
        case characters = "results"
        case next = "next"
        case previous = "previous"
    }
}

extension CharacterListResponse {
    static func mock(items: Int = 10) -> CharacterListResponse {
        var characters = [Character]()
        for _ in 0..<items {
            characters.append(Character.mock())
        }
        return CharacterListResponse(characters: characters, next: nil, previous: nil)
    }
}
