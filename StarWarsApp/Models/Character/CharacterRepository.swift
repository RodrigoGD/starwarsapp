//
//  CharacterRepository.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 18/7/21.
//

import Foundation
import Combine
import CoreData

class CharacterRepository {
    let apiClient: ApiClient!
    let persistentController: PersistenceController!

    init(apiClient: ApiClient = ApiClient(),
         persistentController: PersistenceController = PersistenceController.shared) {
        self.apiClient = apiClient
        self.persistentController = persistentController
    }
}

// MARK: - API Calls
extension CharacterRepository {
    func getAllCharacters() -> AnyPublisher<CharacterListResponse, Error> {
        let url = Constants.baseURL + Constants.characterAppend
        guard let urlRequest = URL(string: url) else {
            return AnyPublisher(Fail<CharacterListResponse, Error>(error: URLError(.badURL)))
        }
        let request = URLRequest(url: urlRequest)
        return apiClient.run(request)
            .map(\.value)
            .eraseToAnyPublisher()
    }

    func getCharacter(from url: String) -> AnyPublisher<Character, Error> {
        guard let requestUrl = URL(string: url) else {
            return AnyPublisher(Fail<Character, Error>(error: URLError(.badURL)))
        }
        let request = URLRequest(url: requestUrl)

        return apiClient.run(request)
            .map(\.value)
            .eraseToAnyPublisher()
    }
}

// MARK: - Persistance Calls
extension CharacterRepository {
    func fetch(character: Character) -> Character {
        return fetchCharacter(from: character.url) ?? character
    }

    func fetchCharacter(from url: String) -> Character? {
        guard let characterId = Utils.extractId(from: url) else {
            print("No character id could be extract from url")
            return nil
        }

        guard let fetchedCharacter = fetchCharacterBy(identifier: characterId) else {
            print("No result for character identifier")
            return nil
        }

        return Character.map(from: fetchedCharacter)
    }

    func hasData(for character: Character) -> Bool {
        guard let _ = fetchCharacter(from: character.url) else {
            return false
        }
        return true
    }

    func save(character: Character) -> AnyPublisher<Character, Error> {
        guard let identifier = Utils.extractId(from: character.url) else {
            let error = NSError(domain: "", code: 0,
                                userInfo: [NSLocalizedDescriptionKey: "Character could not be saved"])
            return AnyPublisher(Fail<Character, Error>(error: error))
        }
        let newCharacter = CharacterDB(context: persistentController.container.viewContext)
        newCharacter.id = identifier
        newCharacter.url = character.url
        newCharacter.name = character.name
        newCharacter.gender = character.gender
        newCharacter.birthYear = character.birthYear
        newCharacter.eyeColor = character.eyeColor
        newCharacter.films = character.films
        newCharacter.hairColor = character.hairColor
        newCharacter.height = character.height
        newCharacter.mass = character.mass
        newCharacter.homeWorld = character.homeWorld
        newCharacter.skinColor = character.skinColor
        newCharacter.species = character.species
        newCharacter.films = character.films
        newCharacter.starships = character.starships
        newCharacter.vehicles = character.vehicles
        if !saveContext() {
            let error = NSError(domain: "", code: 0,
                                userInfo: [NSLocalizedDescriptionKey: "Character could not be saved"])
            return AnyPublisher(Fail<Character, Error>(error: error))
        }
        let subject = CurrentValueSubject<Character, Error>(character)
        return subject.eraseToAnyPublisher()
    }

    func edit(character: Character) -> AnyPublisher<Character, Error> {
        guard let identifier = Utils.extractId(from: character.url),
            let fetchedCharacter = fetchCharacterBy(identifier: identifier) else {
            let error = NSError(domain: "", code: 0,
                                userInfo: [NSLocalizedDescriptionKey: "Character could not be edited"])
            return AnyPublisher(Fail<Character, Error>(error: error))
        }
        fetchedCharacter.id = identifier
        fetchedCharacter.url = character.url
        fetchedCharacter.name = character.name
        fetchedCharacter.gender = character.gender
        fetchedCharacter.birthYear = character.birthYear
        fetchedCharacter.eyeColor = character.eyeColor
        fetchedCharacter.films = character.films
        fetchedCharacter.hairColor = character.hairColor
        fetchedCharacter.height = character.height
        fetchedCharacter.mass = character.mass
        fetchedCharacter.homeWorld = character.homeWorld
        fetchedCharacter.skinColor = character.skinColor
        fetchedCharacter.species = character.species
        fetchedCharacter.films = character.films
        fetchedCharacter.starships = character.starships
        fetchedCharacter.vehicles = character.vehicles
        if !saveContext() {
            let error = NSError(domain: "", code: 0,
                                userInfo: [NSLocalizedDescriptionKey: "Character could not be edited"])
            return AnyPublisher(Fail<Character, Error>(error: error))
        }
        let subject = CurrentValueSubject<Character, Error>(character)
        return subject.eraseToAnyPublisher()
    }

    private func saveContext() -> Bool {
        do {
            try persistentController.container.viewContext.save()
            return true
        } catch {
            let nsError = error as NSError
            fatalError("Unresolved error \(nsError), \(nsError.userInfo)")
        }
    }

    private func fetchCharacterBy(identifier: String) -> CharacterDB? {
        let fetchRequest: NSFetchRequest<CharacterDB>
        fetchRequest = CharacterDB.fetchRequest()

        fetchRequest.predicate = NSPredicate(
            format: "id LIKE %@", identifier
        )

        let context = persistentController.container.viewContext

        do {
            let fetchedCharacters = try context.fetch(fetchRequest)
            guard !fetchedCharacters.isEmpty, let fetchedCharacter = fetchedCharacters.first else {
                return nil
            }
            return fetchedCharacter
        } catch {
            print(error)
            return nil
        }
    }
}
