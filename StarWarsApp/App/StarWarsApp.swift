//
//  StarWarsApp.swift
//  StarWarsApp
//
//  Created by Rodrigo Gálvez on 17/7/21.
//

import SwiftUI

@main
struct StarWarsApp: App {
    @Environment(\.colorScheme) var systemColorScheme
    @State var appColorScheme: ColorScheme? = {
        let colorUserDefaults = UserDefaults.standard.integer(forKey: "userColorScheme")
        guard let userInterfaceStyle = UIUserInterfaceStyle(rawValue: colorUserDefaults) else {
            return nil
        }
        return ColorScheme(userInterfaceStyle)
    }()

    init() {
        UINavigationBar.appearance().backgroundColor = UIColor(named: "BackgroundColor")
        UINavigationBar.appearance().tintColor = UIColor(named: "AccentColor")
        UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: UIColor(named: "TextColor") as Any]
        UINavigationBar.appearance().prefersLargeTitles = true

    }

    var body: some Scene {
        WindowGroup {
            MainView(appColorScheme: $appColorScheme)
        }
    }
}
